import express from 'express'
import cors from 'cors';
import Connection from './db/db.js';
import userRoutes from './routes/user.js';
import cookieParser from 'cookie-parser'

const PORT = 8000;

const app = express();

// configurations 
app.use(express.json());
app.use(cors())
app.use(cookieParser())

// routes 
app.use("/auth", userRoutes)

// database connection 
Connection()

// starting the express server 
app.listen(PORT, () => console.log(`Server running on port ${PORT}`))